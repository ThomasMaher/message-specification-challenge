I used React with redux to solve the message-specification challenge.
Redux maintains the application state while React manages the separate
components.
Sample State Structure (also inlcuded under ```app/Reducers/root_reducer```):
```
{
messagesById: {
  name: "",
  segments: [
      "",
      {
        messageId: id,
        name: messageName
      }
  ],
 dependents: [id, ],
},
messagesIds: [id, ],
editingIds: [id, ],
messageRunning: false || id
}
```

## Run the program
Clone or download repo.
Navigate to root directory in terminal and install dependencies
```
npm install
```
Copy the full path of ```app/index.html``` and paste in browser
to view the program.

## Using the program
I separated the app into three separate functions -
  1. Creating a new message
  2. Viewing / Editing / Deleting Existing messages
  3. Viewing the output of a message that has been run

To create a new message a name must be declared and at least one piece
of non-empty text OR a reference to an exiting message must be added.
The first message must contain text since no message exists to be referenced yet. Each message can contain *a series* of text inputs and/or
message references. Each segment in the series will be displayed as either
the plain text or the name of the reference. Segments can be deleted and
added even after a message has already been created. Segments maintain order
upon deletion. If a message is deleted, any other message that is dependent
on the deleted message will also be deleted.

## Areas for improvement
Messages are mutable only in that the name can be changed and segments
can be added or deleted. With more time I would have liked to also include
the ability to edit text segments in their place as well as to change the
order of existing segments.

Ensuring that deletion will cascade and delete all dependent messages was
a fun puzzle. However, my solution requires an iteration through all
dependent messages followed by a search through the full list of of existing
unique message ids. To improve time, it may have been better to use a structure
that would allow for constant lookup time when deleting messages.
i.e. ```{id1: false, id2: true}``` > ```[id1, id2]```
Even though this would have cluttered the state, it would not be noticed.
