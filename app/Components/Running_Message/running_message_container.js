import { connect } from 'react-redux';

import { deactivateMessage } from '../../actions';
import { getRunningMessage } from '../../util';

import RunningMessageView from './running_message_view';

const mapStateToProps = ( state ) => {
  const { messagesById, runningMessageId } = state;
  if (runningMessageId) {
    return {
      runningMessage: getRunningMessage(
        messagesById,
        messagesById[runningMessageId].segments
      )
    }
  }
  else {
    return {
      runningMessageId
    }
  }
};

const mapDispatchToProps = ( dispatch ) => ({
  deactivateMessage: () => dispatch(deactivateMessage())
});

export default connect(
  mapStateToProps, mapDispatchToProps
)(RunningMessageView);
