import React from 'react';

export default class RunningMessage extends React.Component {
  render() {
    const props = this.props;
    return (
      <div className="div-container">
        <h2>Message Output</h2>
          <div className="running-message">
            <h2>{props.runningMessage}</h2>
          </div>
          {
            (props.runningMessage)
            ? <button onClick={props.deactivateMessage}>Remove</button>
            : null
          }

      </div>
    )
  }
}
