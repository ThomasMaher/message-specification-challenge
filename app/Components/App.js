import React from 'react';

export default function App({ children }) {

  return(
    <div className='app'>
      <h1>Shearwater - message-specification challenge</h1>
      <section>
        { children }
      </section>
    </div>
  )
}
