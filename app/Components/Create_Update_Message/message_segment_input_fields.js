import React from 'react';

export default class MessageSegmentInputFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      ref: {},
    }
  }

  handleChangeText = (e) => {
    this.setState({text: e.target.value});
  }

  handleChangeRef = (e) => {
    let ref = e.target
    const messageRef = {
      messageId: ref.value,
      name: ref.textContent.slice(1, ref.textContent.length)
    }
    if (this.isValidRef(messageRef)) {
      this.setState({ref: messageRef});
    }
  }

  isValidRef(messageRef) {
    const errors = [];
    if (messageRef === undefined
        || messageRef["messageId"] === undefined
        || messageRef["name"] === undefined) {
      errors.push("No valid reference selected");
    }

    if (errors.length === 0) { return true; }
    else { this.props.setErrors(errors); }
  }

  addTextSegment = () => {
    if (this.state.text.length === 0) {
      this.props.errors(["Add content before adding a text segment to your message"]);
    } else {
      this.props.addSegment(this.state.text);
      this.setState({text: ""});
    }
  }

  addRefSegment = () => {
    const ref = this.state.ref;
    if (this.isValidRef(ref)) {
      this.props.addSegment(ref, ref.messageId);
    }
  }

  addTextSegment = () => {
    if (this.state.text.length === 0) {
      this.props.setErrors(["Add content before adding a text segment to your message"]);
    } else {
      this.props.addSegment(this.state.text);
      this.setState({text: ""});
    }
  }

  render() {
    const props = this.props.props;
    const state = this.state;
    return (
      <div className="segment-inputs">
        <section>
        <div>
          <input
            type="text"
            onChange={this.handleChangeText}
            value={state.text} /><br />

            <button
            onClick={() => this.addTextSegment()}>
            Add Text</button>
        </div>

        <div>
          <select onChange={this.handleChangeRef}>
          <option>-</option>
          {props.messageIds.map( id =>
            <option value={id} key={id}>
            {props.messagesById[id].name}
            </option>
          )}
          </select><br />

          <button
            onClick={() => this.addRefSegment()}>
            Add Reference</button>
        </div>

        </section>
      </div>
    )
  }
}
