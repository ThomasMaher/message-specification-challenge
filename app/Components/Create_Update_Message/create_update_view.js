import React from 'react';

// import MessageSegment from './message_segments';
import { getReadableSegmentsByName } from '../../util';

import MessageSegmentInputFields from './message_segment_input_fields';

export default class CreateUpdateView extends React.Component {
  state = {
    message: {
      name: "",
      segments: [],
      dependents: []
    },
    errors: null,
    create: true,
  }

  componentWillMount() {
    if (this.props.existingMessage) {
      this.setState({
        message: this.props.existingMessage,
        create: false,
      });
    }
  }

  setErrors = (errors) => {
    this.setState({errors});
  }

  handleNameChange = (e) => {
    this.setState({
      message: {
        ...this.state.message,
        name: e.target.value
      }
    });
  }

  addSegment = (segment, id) => {
    this.setState({
      message: {
        ...this.state.message,
        segments: [
          ...this.state.message.segments,
          segment
        ],
      }
    });
  }

  deleteSegment = (index) => {
    const segments = this.state.message.segments;
    this.setState({
      message: {
        ...this.state.message,
        segments: [
          ...segments.slice(0, index),
          ...segments.slice(index + 1, segments.length)
        ],
      },
    })
  }

  createMessage = () => {
    const state = this.state;
    if (this.isValid(state.message)) {
      this.props.generateMessage(this.state.message);
      this.setState({
        message: {
          ...this.state.message,
          name: "",
          segments: [],
        },
        errors: null,
      })
    }
  }

  saveChanges = () => {
    const state = this.state;
    if (this.isValid(state.message)) {
      this.props.saveChanges(this.state.message);
    }
  }

  isValid(message) {
    const errors = [];

    if (message.segments.length === 0) {
      errors.push("Please add at least one valid input");
    }
    if (message.name.length === 0) {
      errors.push("The message must be given a name before it can be created");
    }

    if (errors.length === 0) {
      return true;
    } else {
      this.setErrors(errors);
    }
  }

  render() {
    const state = this.state;
    const messages = [];
    const printedSegments = getReadableSegmentsByName(state.message.segments)
    const deleteSegmentAtIndex = index => () => this.deleteSegment(index);

    for (let i = 0; i < printedSegments.length; i++) {
      messages.push(
        <li key={i}>
          <p>{printedSegments[i]}</p>

          <p onClick={deleteSegmentAtIndex(i)}>
            X
          </p>
        </li>
      )
    }

    let createButton = null;
    let cancelButton = null;
    if (state.create) {
      createButton = <button onClick={this.createMessage}>Create</button>
    } else {
      createButton = <button onClick={this.saveChanges}>Update</button>
      cancelButton = <button onClick={this.props.removeFromEditing}>Cancel Changes</button>
    }
    return(
      <section className="section-container">
        <div>
          <h2>{state.create ? "Create New Message" : this.state.name}</h2>
          <section className="errors">{state.errors}</section>
          <section>
            <p>Name:</p>
            <input
              type="text"
              value={state.message.name}
              onChange={this.handleNameChange} />
          </section>

          <p>Add a piece of text or a reference to another message</p>

          <section>
            <MessageSegmentInputFields
              addSegment={this.addSegment}
              props={this.props}
              setErrors={this.setErrors} />
          </section>

          <ul>
            {messages}
          </ul>

          {createButton}
          {cancelButton}
        </div>
      </section>
    )
  }
}
