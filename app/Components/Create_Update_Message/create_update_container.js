import { connect } from 'react-redux';
import shortid from 'shortid';

import {
  setMessage, addIdToList, removeFromEditing, addDependent
} from '../../actions';

import CreateUpdateView from './create_update_view';

const mapStateToProps = (state) => ({
  messagesById: state.messagesById,
  messageIds: state.messageIds,
  editingIds: state.editingIds
});

const mapDispatchToProps = ( dispatch ) => ({
  generateMessage: message => {
    let id = shortid.generate();
    message.segments.forEach(segment => {
      if (typeof segment == 'object') {
        dispatch(addDependent(segment.messageId, id));
      }
    })
    dispatch(setMessage(id, message));
    dispatch(addIdToList(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateUpdateView);
