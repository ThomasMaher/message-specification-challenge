import React from 'react';

import { getReadableSegmentsByName } from '../../util';

import CreateUpdate from '../Create_Update_Message/create_update_container';
import Message from './message';


export default class ReadDeleteMessages extends React.Component {
  render() {
    const props = this.props;
    return (
      <section className="crud-container">
        <CreateUpdate existingMessage={false} />

        <div>
        <h2>Messages List</h2>
          {props.messageIds.map(id => {
            const message = props.messagesById[id];

            if (props.editingIds.includes(id)) {
              return <CreateUpdate
                key={id}
                existingMessage={message}
                removeFromEditing={props.removeFromEditing(id)}
                saveChanges={props.saveChanges(id)} />
            } else {
              return <Message
                key={id}
                name={message.name}
                segments={getReadableSegmentsByName(message.segments)}
                editMessage={props.editMessage(id)}
                deleteMessages={props.deleteMessages(id, props.messagesById)}
                runMessage={props.runMessage(id)}/>
            }
          })}
        </div>

      </section>
    )
  }
}
