import React from 'react';

export default class Message extends React.Component {
  render() {
    return(
      <div className="message-unit">
        <section>
          <h2>{this.props.name}</h2>
        </section>

        <ul style={{marginLeft: "75px"}}>
          {this.props.segments.map( segment =>
            <li key={segment}>
              <p>{segment}</p>
            </li>
          )}
        </ul>

        <section>
          <p onClick={this.props.editMessage}>Edit</p>

          <p onClick={this.props.deleteMessages}>Delete</p>

          <p onClick={this.props.runMessage}>Run</p>
        </section>
      </div>
    )
  }
}
