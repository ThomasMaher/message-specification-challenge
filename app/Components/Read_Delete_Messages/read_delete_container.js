import { connect } from 'react-redux';
import shortid from 'shortid';

import {
  editMessage, setMessage, removeFromEditing,
  saveChanges, deleteMessage, runMessage
 } from '../../actions';
import { deleteDependents } from '../../util';

import ReadDeleteMessages from './read_delete_view';

const mapStateToProps = (state) => ({
  messagesById: state.messagesById,
  messageIds: state.messageIds,
  editingIds: state.editingIds
});

const mapDispatchToProps = ( dispatch ) => ({
  editMessage: (id) => () => dispatch(editMessage(id)),
  removeFromEditing: (id) => () => dispatch(removeFromEditing(id)),
  deleteMessages: (id, messagesById) => () => {
    deleteDependents(id, messagesById, id => dispatch(deleteMessage(id)));
  },
  runMessage: (id) => () => dispatch(runMessage(id)),
  saveChanges: (id) => (message) => {
    dispatch(setMessage(id, message));
    dispatch(removeFromEditing(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ReadDeleteMessages);
