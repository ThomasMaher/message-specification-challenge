import React from 'react';

export default class MessageSegment extends React.Component {
  render() {
    let segment = this.props.messageSegment;

    return(
      <div style={{display: "flex"}}>
        <input
          type="text"
          value={segment}
          onChange={(e) => this.props.handleMessageUpdate(e.target.value)} />

        <h4>Or</h4>

        <select onChange={e => this.props.handleSelect(e.target.value)}>
          <option value={null}>-</option>
          {this.props.messagesByName}
        </select>
      </div>
    )
  }
}
