import merge from 'lodash/merge';

import { RUN_MESSAGE, REMOVE_MESSAGE } from '../actions';

const defaultState = false;

export function runningMessageReducer(state = defaultState, action) {
  switch (action.type) {
    case RUN_MESSAGE:
      return action.id;
    case REMOVE_MESSAGE:
      console.log("in reducer");
      return false;
    default:
      return state;
  }
}
