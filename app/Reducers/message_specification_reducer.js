import merge from 'lodash/merge';

import { SET_MESSAGE, ADD_DEPENDENT } from '../actions';

const defaultState = {};

export function messageSpecificationReducer(state = defaultState, action) {
  Object.freeze(state);
  const newState = merge({}, state);

  switch (action.type) {
    case SET_MESSAGE:
      newState[action.id] = action.message;
      return newState;
    case ADD_DEPENDENT:
      newState[action.parent].dependents.push(action.dependent);
      return newState;
    default:
      return newState;
  }
}

// messagesById = {
//   id: {
//     name: "",
//     segments: [],
//     dependents: []
//   }
// }
