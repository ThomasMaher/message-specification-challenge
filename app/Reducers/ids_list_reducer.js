import merge from 'lodash/merge';

import { ADD_ID_TO_LIST, DELETE_ID_FROM_LIST} from '../actions';

const defaultState = [];

export function idsListReducer(state = defaultState, action) {
  Object.freeze(state);
  const newState = merge([], state);

  switch (action.type) {
    case ADD_ID_TO_LIST:
      newState.push(action.id);
      return newState;
    case DELETE_ID_FROM_LIST:
      const index = newState.indexOf(action.id);
      const front = newState.slice(0, index);
      const back = newState.slice(index + 1, newState.length)
      return front.concat(back);
    default:
      return newState;
  }
}
