import merge from 'lodash/merge';

import { ADD_TO_EDITING, REMOVE_FROM_EDITING } from '../actions';

const defaultState = [];

export function editingListReducer(state = defaultState, action) {
  Object.freeze(state);
  const newState = merge([], state);

  switch (action.type) {
    case ADD_TO_EDITING:
      newState.push(action.id);
      return newState;
    case REMOVE_FROM_EDITING:
      const index = newState.indexOf(action.id);
      const front = newState.slice(0, index);
      const back = newState.slice(index + 1, newState.length);
      return front.concat(back);
    default:
      return newState;
  }
}
