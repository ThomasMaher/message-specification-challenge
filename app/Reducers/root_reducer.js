import { combineReducers } from 'redux';

import { messageSpecificationReducer } from './message_specification_reducer';
import { runningMessageReducer } from './running_message_reducer';
import { editingListReducer } from './editing_list_reducer';
import { idsListReducer } from './ids_list_reducer';

const RootReducer = combineReducers({
  messagesById: messageSpecificationReducer,
  messageIds: idsListReducer,
  editingIds: editingListReducer,
  runningMessageId: runningMessageReducer,
});

export default RootReducer;

// Reducer Shape = {
//   messagesById: {
//     name: "",
//     segments: [
//         "",
//         {
//           messageId: id,
//           name: messageName
//         }
//     ],
//    dependents: [],
//   },
//   messagesIds: [],
//   editingIds: [],
//   messageRunning: false || id
// }
