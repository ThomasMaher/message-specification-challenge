export const getReadableSegmentsByName = (segments) => {
  return segments.map(segment => (
          typeof segment == 'string')
          ? "Text: " + segment
          : "Ref:  " + segment.name
        )
}

export const getRunningMessage = (messagesById, segments) => {
  let resultingMessage = "";
  segments.forEach( segment => {
    if (typeof segment == 'string') {
      resultingMessage += segment
    } else {
      resultingMessage += getRunningMessage(
        messagesById,
        messagesById[segment.messageId].segments
      )
    }
  });
  return resultingMessage;
}

export const deleteDependents = (id, messagesById, deleteMessage) => {
  messagesById[id].dependents.forEach(dependentId => {
    deleteDependents(dependentId, messagesById, deleteMessage);
  });
  deleteMessage(id);
}
