import React from 'react';
import ReactDOM from 'react-dom';
import merge from 'lodash/merge';

import { configureStore } from './store';
import { Provider } from 'react-redux';

import App from './Components/App';
import CreateUpdate from './Components/Create_Update_Message/create_update_container';
import ReadDelete from './Components/Read_Delete_Messages/read_delete_container';
import RunningMessage from './Components/Running_Message/running_message_container';

const Root = ({ store }) => {
  return (
    <Provider store={ store }>
      <App>
        <ReadDelete />
        <RunningMessage />
      </App>
    </Provider>
  )
}

document.addEventListener('DOMContentLoaded', () => {
  let store = configureStore();
  ReactDOM.render(<Root store={ store }/>, document.getElementById('root'));
})
