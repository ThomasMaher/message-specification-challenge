export const SET_MESSAGE = 'SET_MESSAGE';
export const ADD_DEPENDENT = 'ADD_DEPENDENT';
export const ADD_ID_TO_LIST = 'ADD_ID_TO_LIST';
export const DELETE_ID_FROM_LIST = 'DELETE_ID_FROM_LIST';
export const ADD_TO_EDITING = 'ADD_TO_EDITING';
export const REMOVE_FROM_EDITING = 'REMOVE_FROM_EDITING';
export const RUN_MESSAGE = 'RUN_MESSAGE';
export const REMOVE_MESSAGE = 'REMOVE_MESSAGE';

export const setMessage = (id, message) => ({
  type: SET_MESSAGE,
  id,
  message
});

export const addDependent = (parent, dependent) => ({
  type: ADD_DEPENDENT,
  parent,
  dependent,
})

export const addIdToList = (id) => ({
  type: ADD_ID_TO_LIST,
  id
});

export const deleteMessage = (id) => ({
  type: DELETE_ID_FROM_LIST,
  id
});

export const editMessage = (id) => ({
  type: ADD_TO_EDITING,
  id
});

export const removeFromEditing = (id) => ({
  type: REMOVE_FROM_EDITING,
  id
});

export const runMessage = (id) => ({
  type: RUN_MESSAGE,
  id
});

export const deactivateMessage = () => ({
  type: REMOVE_MESSAGE
});
